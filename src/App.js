import React from 'react';
import heroImg from './images/left_img.jpg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div class="container">
          <div class="left_img">
            <img src={heroImg} alt="#" />
          </div>
          <div class="right_form">
            <div class="right_side">
            <h2><span>cart</span>&order</h2>
            <h3>Change Password</h3>
              <form class="floating-form">
                <div class="floating-label">      
                  <input class="floating-input" type="text" placeholder=" " />
                  <span class="highlight"></span>
                  <label>New Password</label>
                </div>

                <div class="floating-label">      
                  <input class="floating-input" type="text" placeholder=" " />
                  <span class="highlight"></span>
                  <label>Confirm Password</label>
                </div>

                <button type="submit"> Submit</button>
              </form>
          </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
